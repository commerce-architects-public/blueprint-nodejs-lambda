import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { deleteUser } from '/opt/nodejs/services/user-service';

export const handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  console.log(`received event: ${event.requestContext.requestId}`);
  if (
    event.queryStringParameters?.id == '' ||
    event.queryStringParameters?.id === undefined
  ) {
    throw new Error('Missing Required Param');
  }
  const user = await deleteUser(event.queryStringParameters?.id);
  console.log(`delete user: ${JSON.stringify(user)}`);
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    body: JSON.stringify(user)
  } as APIGatewayProxyResult;
};
export default handler;
