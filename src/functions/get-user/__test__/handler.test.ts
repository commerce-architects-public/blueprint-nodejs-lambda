import { handler } from '../handler';
import { apiGatewayEvent } from '../../../events/get-user';
import { expectedUserInfo } from './mocks/user-mocks';
import { APIGatewayProxyEvent } from 'aws-lambda';
describe('get-user handler', () => {
  describe('when get-user handler is called', () => {
    it('it should query user by id and return it', async () => {
      const response = await handler(
        apiGatewayEvent as unknown as APIGatewayProxyEvent
      );
      expect(response).toStrictEqual({
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: JSON.stringify(expectedUserInfo)
      });
    });
    it('it should throw an error if query param is missing', async () => {
      apiGatewayEvent.queryStringParameters.id = '';
      await expect(
        handler(apiGatewayEvent as unknown as APIGatewayProxyEvent)
      ).rejects.toThrow(/Missing Required Param/);
    });
  });
});
