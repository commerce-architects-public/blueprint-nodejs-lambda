import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';

export class deleteUserService extends Construct {
  constructor(
    scope: Construct,
    id: string,
    sharedLayers: lambda.LayerVersion[]
  ) {
    super(scope, id);

    const handler = new lambda.Function(this, 'deleteUserHandler', {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset('build/src/functions/delete-user'),
      layers: sharedLayers,
      handler: 'handler'
    });

    const api = new apigateway.RestApi(this, 'delete-user-api', {
      restApiName: 'Delete User Service',
      description: 'Service to delete a user.'
    });

    const deleteUserIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' }
    });

    api.root.addMethod('DELETE', deleteUserIntegration);
  }
}
