import { handler } from '../handler';
import { apiGatewayEvent } from '../../../events/create-user';
import { APIGatewayProxyEvent } from 'aws-lambda';
describe('create-user handler', () => {
  describe('when create-user handler is called', () => {
    it('it should have a valid body', async () => {
      const response = await handler(
        apiGatewayEvent as unknown as APIGatewayProxyEvent
      );
      expect(response).toEqual(
        expect.objectContaining({
          headers: {
            'Content-Type': 'application/json'
          },
          statusCode: 200
        })
      );
    });
    it('it should throw an error body is missing', async () => {
      apiGatewayEvent.body = '';
      await expect(
        handler(apiGatewayEvent as unknown as APIGatewayProxyEvent)
      ).rejects.toThrow(/Missing Request Body/);
    });
  });
});
