import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';

export class createUserService extends Construct {
  constructor(
    scope: Construct,
    id: string,
    sharedLayers: lambda.LayerVersion[]
  ) {
    super(scope, id);

    const handler = new lambda.Function(this, 'createUserHandler', {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset('build/src/functions/create-user'),
      layers: sharedLayers,
      handler: 'handler'
    });

    const api = new apigateway.RestApi(this, 'create-user-api', {
      restApiName: 'Create User Service',
      description: 'Service to create a user.'
    });

    const putUserIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' }
    });

    api.root.addMethod('PUT', putUserIntegration);
  }
}
