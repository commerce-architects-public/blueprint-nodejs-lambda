import axios from 'axios';

import { CreateUserResponse, User } from '../types/types';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export async function getUser(userId: string): Promise<User> {
  console.log(`get-user.getUser called with Id: ${userId}`);
  return await axios
    .get<User>(`https://jsonplaceholder.typicode.com/users/${userId}`)
    .then((response: { data: User }) => {
      console.log(response.data);
      return response.data as User;
    });
}

export async function deleteUser(userId: string): Promise<User> {
  return await axios
    .delete<User>(`https://jsonplaceholder.typicode.com/users/${userId}`)
    .then((response: { data: User }) => {
      return response.data as User;
    });
}

export async function createUser(user: User): Promise<CreateUserResponse> {
  console.log(`get-user.createUser called`);
  return await axios
    .post('https://jsonplaceholder.typicode.com/users', user)
    .then((response: { data: CreateUserResponse }) => {
      return response.data as CreateUserResponse;
    });
}
